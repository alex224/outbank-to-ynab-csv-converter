//
//  Converter.m
//  Outbank-to-YNAB-CSV-Converter
//
//  Created by Alex on 23.03.13.
//  Copyright (c) 2013 bitkiller. All rights reserved.
//

#import "Converter.h"

@implementation Converter

+ (BOOL) convertInputFile:(NSString *) inputFile toOutputFile:(NSString*) outputFile {
    
    NSString * inFileContents = [NSString stringWithContentsOfFile:inputFile encoding:NSUTF8StringEncoding error:nil];
    
    NSMutableString * outFileContents = [NSMutableString new];
    [outFileContents appendString:@"Date,Payee,Category,Memo,Outflow,Inflow\n"];
    
    NSArray *lines = [inFileContents componentsSeparatedByString: @"\n"];
    
    int lineCounter = 0;
    for (NSString *line in lines) {
        //skip first line
        if (lineCounter++ == 0) {
            continue;
        }
        
        NSArray *parts = [line componentsSeparatedByString:@","];
        if (parts.count <= 4) {
            parts = [line componentsSeparatedByString:@";"];
        }
        if (parts.count > 10) {
            NSString* strDate = parts[2];
            NSString* strPayee = parts[5];
            NSString* strBetrag = parts[4];
            NSMutableString *strZweck = [NSMutableString new];
            for (int i = 12; i < parts.count && i < 12+14; i++) {
                [strZweck appendString:parts[i]];
            }
          
            [Converter appendDate:strDate payee:strPayee betrag:strBetrag zweck:strZweck toContent:outFileContents];
    
        }

    }

    [outFileContents writeToFile:outputFile atomically:YES encoding:NSUTF8StringEncoding error:nil];

    return YES;
}

+ (void) appendDate:(NSString*) date payee:(NSString*) payee betrag:(NSString*) betrag zweck:(NSString*) zweck toContent:(NSMutableString*) content {
    
    [content appendString:[date stringByReplacingOccurrencesOfString:@"." withString:@"/"]];
    [content appendString:@","];
    
    [content appendFormat:@"\"%@\"", payee];
    [content appendString:@","];
    
    //category
    [content appendString:@","];
    
    [content appendFormat:@"\"%@\"", zweck];
    [content appendString:@","];

    NSString *outFlow = @"";
    NSString *inFlow = @"";
    
    betrag = [betrag stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    if ([betrag hasPrefix:@"-"]) {
        outFlow = [betrag stringByReplacingOccurrencesOfString:@"," withString:@"."];
        outFlow = [outFlow stringByReplacingOccurrencesOfString:@"-" withString:@""];
    } else {
        inFlow = [betrag stringByReplacingOccurrencesOfString:@"," withString:@"."];
    }
    
    [content appendString:outFlow];
    [content appendString:@","];
    
    [content appendString:inFlow];
    [content appendString:@"\n"];
    
}

@end
