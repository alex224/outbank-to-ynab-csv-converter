//
//  Converter.h
//  Outbank-to-YNAB-CSV-Converter
//
//  Created by Alex on 23.03.13.
//  Copyright (c) 2013 bitkiller. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Converter : NSObject

+ (BOOL) convertInputFile:(NSString *) inputFile toOutputFile:(NSString*) outputFile;

@end
