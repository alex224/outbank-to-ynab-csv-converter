//
//  main.m
//  Outbank-to-YNAB-CSV-Converter
//
//  Created by Alex on 23.03.13.
//  Copyright (c) 2013 bitkiller. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Converter.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        
        NSString *inputFile = nil;
        NSString *outputFile = nil;
        
        if (argc <= 1) {
            NSLog(@"Argument <input file> is missing.");
            return -1;
            
        } else {
            inputFile = [NSString stringWithCString:argv[1] encoding:NSUTF8StringEncoding];
            
            BOOL directory = NO;
            if (![fm fileExistsAtPath:inputFile isDirectory:&directory]) {
                NSLog(@"Input file %@ not found.", inputFile);
                return -2;
            } else if (directory) {
                NSLog(@"Input file %@ is a directory.", inputFile);
                return -6;
            }
        }
        
        
        if (argc <= 2) {
            NSLog(@"Argument <output file> is missing.");
            return -3;
        } else {
            outputFile = [NSString stringWithCString:argv[2] encoding:NSUTF8StringEncoding];
        }
        
        [Converter convertInputFile:inputFile toOutputFile:outputFile];
    
    }
    return 0;
}

